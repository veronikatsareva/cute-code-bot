#  -*- coding: utf-8 -*-
import re


def transp(string, command, shift):
    # в зависимости от команды вызывается функция кодирования/декодирования
    if command == "кодировать":
        return trans_encoding(string, shift)
    elif command == "декодировать":
        return trans_decoding(string, shift)
    # иначе пользователь ввел такие данные, с которыми мы не можем работать
    return (
        "Я не знаю такой команды. Пожалуйста, попробуйте снова. "
        "Для справки введите команду /help."
    )


def trans_encoding(string, shift):
    if not re.search(r"[А-я]|[A-z]|[Ё-ё]", rf"{string}"):
        return "Шифр работает только со строками, содержащими латинские или кириллические символы."
    else:
        z = list(string)
        l = len(z)
        q = 0
        m = []
        answer = ""
        # пока не дошли до конца введённой строки
        while l > 0:
            for i in range(shift):
                m.append([])
                # случай, когда l % b != 0
                if i < l:
                    m[i].append(z[i + q])
            # сдвиг на следющие b элементов
            l -= shift
            # q отмеряет, в какой мы b-шке
            q += shift
        # чтобы одной строкой красиво вывести
        for a in range(shift):
            answer = answer + "".join(m[a])
    return answer


# аналогично encoding
def trans_decoding(string, shift):
    if not re.search(r"[А-я]|[A-z]|[Ё-ё]", rf"{string}"):
        return "Шифр работает только со строками, содержащими латинские или кириллические символы."
    else:
        z = list(string)
        q = len(z)
        x = []
        m = 0
        answer = ""
        # width - ширина прямоугольника, length - длина, если есть одна неполная строчка - длина - 1
        if q % shift == 0:
            width = q // shift
        else:
            width = q // shift + 1
        while q > 0:
            for a in range(width):
                x.append([])
                if a < q:
                    x[a].append(z[a + m])
            m += width
            q -= width
        for a in range(width):
            answer = answer + "".join(x[a])
        return answer
