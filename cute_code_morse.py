from moviepy.editor import concatenate_audioclips, AudioFileClip

# Данные для кодирования
cyr_code_morse = {
    "а": "·-",
    "б": "-···",
    "в": "·--",
    "г": "--·",
    "д": "-··",
    "е": "·",
    "ж": "···-",
    "з": "--··",
    "и": "··",
    "й": "·---",
    "к": "-·-",
    "л": "·-··",
    "м": "--",
    "н": "-·",
    "о": "---",
    "п": "·--·",
    "р": "·-·",
    "с": "···",
    "т": "-",
    "у": "··-",
    "ф": "··-·",
    "х": "····",
    "ц": "-·-·",
    "ч": "---·",
    "ш": "----",
    "щ": "--·-",
    "ъ": "·--·-·",
    "ы": "-·--",
    "ь": "-··-",
    "э": "··-··",
    "ю": "··--",
    "я": "·-·-",
    ".": "······",
    ",": "·-·-·-",
    "!": "--··--",
}

lat_code_morse = {
    "a": "·-",
    "b": "-···",
    "c": "-·-·",
    "d": "-··",
    "e": "·",
    "f": "··-·",
    "g": "--·",
    "h": "····",
    "i": "··",
    "j": "·---",
    "k": "--·--",
    "l": "·-··",
    "m": "--",
    "n": "-·",
    "o": "---",
    "p": "·--·",
    "q": "--·-",
    "r": "·-·",
    "s": "···",
    "t": "-",
    "u": "··-",
    "v": "···-",
    "w": "·--",
    "x": "-··-",
    "y": "-·--",
    "z": "--··",
    ".": "·-·-·-",
    ",": "--··--",
    "!": "-·-·--",
}

other_sym_code_morse = {
    "0": "-----",
    "1": "·----",
    "2": "··---",
    "3": "···--",
    "4": "····-",
    "5": "·····",
    "6": "-····",
    "7": "--···",
    "8": "---··",
    "9": "----·",
    "?": "··--··",
    "'": "·----·",
    "/": "-··-·",
    "(": "-·--·",
    ")": "-·--·-",
    "&": "·-···",
    ":": "---···",
    ";": "-·-·-·",
    "=": "-···-",
    "+": "·-·-·",
    "—": "-····-",
    "_": "··--·-",
    '"': "·-··-·",
    "$": "···-··-",
    "@": "·--·-·",
    "¿": "··-·-",
    "¡": "--···-",
    "×": "-··-",
    "%": "----- -··-· -----",
}


# пользователь выбирает режим работы: кодировать/раскодировать
def get_function(function, message, language, final_format):
    # реализация режима кодировки
    if function == "кодировать":
        message = message.lower().replace("ё", "е").replace("-", "—")

        # алфавиты для проверки
        cyr_alph = set("абвгдеёжзийклмнопрстуфхцчшщъыьэюя")
        lat_alph = set("abcdefghijklmnopqrstuvwxyz")
        other_symbols = set("0123456789.,?'!/()&:;=+-—_\"$@¿¡×%\n ")

        # проверка на недопустимые символы
        if not set(message.lower()) <= set.union(cyr_alph, lat_alph, other_symbols):
            return "В сообщении есть недопустимые символы. Для справки введите команду /help."
            # message = input(f'{info}').lower().replace("ё", "е").replace('-', '—')

        # проверка на единство алфавита: только кириллица или только латиница
        if len(set(message) & lat_alph) != 0 and len(set(message) & cyr_alph) != 0:
            return "В сообщении есть и кириллические, и латинские буквы. Для справки введите команду /help."
            # message = input(f'{info}').lower().replace("ё", "е").replace('-', '—')

        # итоговое сообщение -- список списков: message[i][x], где [i] -- последовательность символов между пробелами,
        # [x] -- символ в последовательности
        message_net = message.split(" ")
        for word in range(0, len(message_net)):
            message_net[word] = list(message_net[word])

        # присвоение языка сообщению для вызова нужной функции
        if len(set(message) & cyr_alph) != 0:
            return get_cyr_code_morse(function, message_net, final_format)

        elif len(set(message) & lat_alph) != 0:
            return get_lat_code_morse(function, message_net, final_format)

        # некоторые знаки пунктуации в кириллице и латинице различаются
        elif len(set(message) & cyr_alph & lat_alph) == 0:
            language = language.lower()
            if language == "латиница":
                return get_lat_code_morse(function, message_net, final_format)
            elif language == "кириллица":
                return get_cyr_code_morse(function, message_net, final_format)

    # реализация режима расшифровки
    elif function == "раскодировать":
        message = message.replace(".", "·")

        if not set(message) <= {".", "-", " ", "\t", "·"}:
            return "Ваше сообщение содержит недопустимые символы. Для справки введите команду /help."
            # message = input(f'{info}').replace('.', '·')

        # подготовка сообщения к расшифровке
        message = message.split(" ")
        for element in range(0, len(message)):
            if message[element] != "":
                continue
            else:
                message[element] = " "

        # выбор языка для дальнейшего вызова функции
        language = language.lower()
        if language == "латиница":
            return get_lat_symbols(function, message, final_format)
        elif language == "кириллица":
            return get_cyr_symbols(function, message, final_format)


# Расшифровка кода Морзе. Кириллица/латиница/другие символы (числа и пунктуация)
def get_cyr_symbols(function, message, final_format):
    cyr_code_morse_reverse = dict(zip(cyr_code_morse.values(), cyr_code_morse.keys()))
    for symbol in range(0, len(message)):
        if message[symbol] not in cyr_code_morse_reverse.keys():
            continue
        message[symbol] = cyr_code_morse_reverse[message[symbol]]
    return get_other_symbols(function, message, final_format)


def get_lat_symbols(function, message, final_format):
    lat_code_morse_reverse = dict(zip(lat_code_morse.values(), lat_code_morse.keys()))
    for symbol in range(0, len(message)):
        if message[symbol] not in lat_code_morse_reverse.keys():
            continue
        message[symbol] = lat_code_morse_reverse[message[symbol]]
    return get_other_symbols(function, message, final_format)


def get_other_symbols(function, message, final_format):
    other_sym_code_morse_reverse = dict(
        zip(other_sym_code_morse.values(), other_sym_code_morse.keys())
    )
    for symbol in range(0, len(message)):
        if message[symbol] not in other_sym_code_morse_reverse.keys():
            continue
        message[symbol] = other_sym_code_morse_reverse[message[symbol]]
    return to_get_format(function, message, final_format)


# Кодирование латиницы, кириллицы и других символов в код Морзе
# Кодировка кириллического алфавита и некоторых символов пунктуации
def get_cyr_code_morse(function, message_net, final_format):
    for el in message_net:
        for symbol in range(0, len(el)):
            if el[symbol] not in cyr_code_morse.keys():
                continue
            el[symbol] = cyr_code_morse[el[symbol]]
    return get_other_sym_code_morse(function, message_net, final_format)


# Кодировка латинского алфавита и некоторых символов пунктуации
def get_lat_code_morse(function, message_net, final_format):
    for el in message_net:
        for symbol in range(0, len(el)):
            if el[symbol] not in lat_code_morse.keys():
                continue
            else:
                el[symbol] = lat_code_morse[el[symbol]]
    return get_other_sym_code_morse(function, message_net, final_format)


# # Кодировка цифр и других знаков пунктуации
def get_other_sym_code_morse(function, message_net, final_format):
    for el in message_net:
        for symbol in range(0, len(el)):
            if el[symbol] not in other_sym_code_morse.keys():
                continue
            else:
                el[symbol] = other_sym_code_morse[el[symbol]]
    return to_get_format(function, message_net, final_format)


# пользователь определяет формат итогового сообщения: сообщение в чате, txt-файл или аудио-файл
# (аудио-файл недоступен в формате расшифровки)
def to_get_format(function, message, final_format):
    if function == "кодировать":
        # Итоговый вид сообщения для кода Морзе
        for el in range(0, len(message)):
            message[el] = " ".join(message[el])
        final_code_morse = "   ".join(message)
        return to_send_mes(final_format, final_code_morse)

    elif function == "раскодировать":
        # Итоговый вид сообщения для расшифрованного текста
        final_message = "".join(message).replace("  ", " ")
        return to_send_mes(final_format, final_message)


# отправка итогового сообщения пользователю
def to_send_mes(final_format, content):
    # пока только озвучка без записи в wav
    if final_format == "аудиозапись в формате wav":
        clips = []
        for char in content:
            if char == "-":
                clips.append(AudioFileClip("long_sound.wav"))
                clips.append(AudioFileClip("short_pause.wav"))
            elif char == "·":
                clips.append(AudioFileClip("short_sound.wav"))
                clips.append(AudioFileClip("short_pause.wav"))
            else:
                clips.append(AudioFileClip("long_pause.wav"))

        final_clip = concatenate_audioclips(clips)
        return final_clip.write_audiofile("morse.wav")

    # отправка сообщения
    elif final_format == "сообщение в чате":
        return content

    # запись в txt-файл
    elif final_format == "txt-файл":
        with open("morse.txt", "w", encoding="UTF-8") as fw:
            return fw.write(content)
