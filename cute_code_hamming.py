import math


# функция проверки введенных данных, строка должна состоять только из 0 и 1
def checker(string):
    for char in string:
        if char not in ["0", "1"]:
            return False
    return True


# основная функция, которая будет вызываться в cute_code_main в теле бота,
# принимает в качестве аргументов строку, команду для кодирования/декодирования
# также производит проверку введенных данных
def hamming(string, command):
    if command == "кодировать":
        if not checker(string) or len(string) < 4:
            return (
                "Данные такого формата на поддерживаются. "
                "Пожалуйста, попробуйте снова. "
                "Для справки введите команду /help."
            )
        else:
            return hamming_code(string)
    elif command == "раскодировать":
        if not checker(string) or len(string) < 7:
            return (
                "Данные такого формата на поддерживаются. "
                "Пожалуйста, попробуйте снова. "
                "Для справки введите команду /help."
            )
        else:
            return hamming_decode(string)
    return (
        "Я не знаю такой команды. Пожалуйста, попробуйте снова. "
        "Для справки введите команду /help."
    )


# функция кодировки
def hamming_code(string):
    # массив, куда будут записываться биты
    # в качестве длины берем ближайший (снизу) целый логарифм + 1 по основанию
    # 2, чтобы понять сколько контрольных битов будет в строке, и добавляем
    # еще одну клетку, чтобы избежать проблем со сдвигом
    code_string = [0] * (int(math.log2(len(string))) + len(string) + 2)

    # список контрольных битов, то есть степеней двойки
    checkers = []
    for i in range(1, len(string) + 1):
        if math.log2(i) == int(math.log2(i)):
            checkers.append(i)

    # не контрольные биты заполняем битами из строки
    j = 0
    for idx in range(1, len(code_string)):
        if idx not in checkers:
            code_string[idx] = int(string[j])
            j += 1

    # производим подсчет контрольных сумм
    for elem in checkers:
        cur_sum = 0
        # начинаем с контрольного бита и идем с шагом текущей степени двойки
        for i in range(elem, len(code_string), 2 * elem):
            # считаем сумму битов в каждом диапазоне, на случай если диапазон
            # меньше, чем степень двойки, берем в качестве правой границы
            # минимум из длины строки и предполагаемого конца диапазона
            for j in range(i, min(len(code_string), i + elem)):
                cur_sum += code_string[j]
        # записываем остаток от деления на 2 контрольной суммы в
        # соответствующий контрольный бит
        code_string[elem] = cur_sum % 2

    # преобразовываем список интов в список строк
    code_string = list(map(str, code_string))

    # выводим все с первого элемента (потому что нулевой нужен был для
    # выравнивания сдвига)
    return "".join(code_string[1:])


# функция декодировки
def hamming_decode(string):
    # превращение закодированной строки в список интов и добавление нулевого
    # элемента в начало для выравнивания сдвига
    coded_string = list(map(int, string))
    coded_string.insert(0, 0)
    decode_string = []

    # список контрольных битов, то есть степеней двойки
    checkers = []
    for i in range(1, len(string) + 1):
        if math.log2(i) == int(math.log2(i)):
            checkers.append(i)

    # индекс бита в котором может быть ошибка
    mistake = 0

    # проверка контрольных сумм
    for elem in checkers:
        cur_sum = 0
        for i in range(elem, len(coded_string), 2 * elem):
            for j in range(i, min(len(coded_string), i + elem)):
                cur_sum += coded_string[j]
        # если контрольная сумма не совпадает с тем, что написано в контрольном
        # бите мы складываем индексы ошибочных битов в mistake
        if coded_string[elem] != (cur_sum - int(coded_string[elem])) % 2:
            mistake += elem
    # если бит не превосходит длины строки, то ошибка одна
    # в случае если ошибок нет, мы будем менять нулевой элемент, который мы
    # не учитываем
    if mistake <= len(coded_string):
        # меняем элемент в бите на противоположный
        coded_string[mistake] = coded_string[mistake] ^ 1
    # иначе ошибок больше одной, мы не можем дешифровать последовательность
    else:
        return (
            "В данном коде более одной ошибки. Я не могу его "
            "дешифровать. Попробуйте снова. "
            "Для справки введите команду /help."
        )

    # записываем в отдельный массив не контрольные биты
    for i in range(len(coded_string)):
        if i not in checkers:
            decode_string.append(str(coded_string[i]))

    # выводим все с первого элемента (потому что нулевой нужен был для
    # выравнивания сдвига)
    return "".join(decode_string[1:])
