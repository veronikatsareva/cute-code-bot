# основная функция, которая будет вызываться в cute_code_main в теле бота,
# принимает в качестве аргументов строку, команду
# для кодирования/декодирования, сдвиг, кириллический (без ё)/латинский алфавит
def caesar(string, command, shift, alphabet):
    # в зависимости от команды вызывается функция кодирования/декодирования
    if command == "кодировать":
        return caesar_code(string, shift, alphabet)
    elif command == "раскодировать":
        return caesar_decode(string, shift, alphabet)
    # иначе пользователь ввел такие данные, с которыми мы не можем работать
    return (
        "Я не знаю такой команды. Пожалуйста, попробуйте снова. "
        "Для справки введите команду /help."
    )


# общий алгоритм работы функций шифрования/дешифрования:
# 1. если пользователь ввел некорректный алфавит, то функция будет завершена
# 2. обработать случай когда пользователь ввел число, которое больше
# длины используемого алфавита (строка со взятием остатка сдвига по модулю
# равному длине латинского/кириллического алфавита)
# 3. пройтись циклом по строке -- если символ не буква, то добавить его в
# массив без изменений
# -- если буква, то в зависимости от регистра посчитать номер буквы в
# ascii-таблице после сдвига и записать в массив, отдельно рассматривая случаи с буквами "ё","Ё"
# 4. вернуть строку из объединенных символов из массива с помощью join

# как считается новый индекс символа?
# -- если сдвиг не выходит за рамки индексов первой/последней буквы алфавита,
# то новый индекс получается просто прибавлением (вычитанием) сдвига к индексу
# нынешнего символа
# -- иначе находится количество элементов, на которое сдвиг выходит за рамки
# последнего (первого) символа, и прибавляется (вычитается) к (из) первому
# (последнего) символу(а) алфавита в соотвествующем регистре


# функция шифрования
def caesar_code(string, shift, alphabet):
    # массив для хранения зашифрованных символов
    code_string = []
    if alphabet == "латиница":
        shift = shift % 26
        for char in string:
            if char.isalpha():
                if char.islower():
                    if ord(char) + shift <= ord("z"):
                        code_string.append(chr(ord(char) + shift))
                    else:
                        code_string.append(
                            chr(ord("a") + ord(char) + shift - ord("z") - 1)
                        )
                elif char.isupper():
                    if ord(char) + shift <= ord("Z"):
                        code_string.append(chr(ord(char) + shift))
                    else:
                        code_string.append(
                            chr(ord("A") + ord(char) + shift - ord("Z") - 1)
                        )
            else:
                code_string.append(char)
    elif alphabet == "кириллица":
        shift = shift % 32
        for char in string:
            if char.isalpha():
                if char.islower():
                    if ord(char) + shift <= ord("я"):
                        # отдельно рассматриваем случай с буквой ё
                        if ord(char) == 1105:
                            code_string.append(chr(1077 + shift))
                        else:
                            code_string.append(chr(ord(char) + shift))
                    else:
                        if ord(char) == 1105:
                            code_string.append(
                                chr(ord("а") + 1077 + shift - ord("я") - 1)
                            )
                        else:
                            code_string.append(
                                chr(ord("а") + ord(char) + shift - ord("я") - 1)
                            )
                elif char.isupper():
                    if ord(char) + shift <= ord("Я"):
                        # отдельно рассматриваем случай с буквой Ё
                        if ord(char) == 1023:
                            code_string.append(chr(1045 + shift))
                        else:
                            code_string.append(chr(ord(char) + shift))
                    else:
                        if ord(char) == 1023:
                            code_string.append(
                                chr(ord("А") + 1045 + shift - ord("Я") - 1)
                            )
                        else:
                            code_string.append(
                                chr(ord("А") + ord(char) + shift - ord("Я") - 1)
                            )
            else:
                code_string.append(char)
    else:
        # пользователь ввел неподерживаемый алфавит
        return (
            "Такой алфавит не предусмотрен. Пожалуйста, попробуйте снова."
            "Для справки введите команду /help."
        )
    # возвращение зашифрованной строки
    return "".join(code_string)


# функция дешифрования
def caesar_decode(string, shift, alphabet):
    # массив для хранения расшифрованных символов
    decode_string = []
    if alphabet == "латиница":
        shift = shift % 26
        for char in string:
            if char.isalpha():
                if char.islower():
                    if ord(char) - shift >= ord("a"):
                        decode_string.append(chr(ord(char) - shift))
                    else:
                        decode_string.append(
                            chr(ord("z") - (ord("a") - (ord(char) - shift) - 1))
                        )
                elif char.isupper():
                    if ord(char) - shift >= ord("A"):
                        decode_string.append(chr(ord(char) - shift))
                    else:
                        decode_string.append(
                            chr(ord("Z") - (ord("A") - (ord(char) - shift) - 1))
                        )
            else:
                decode_string.append(char)
    elif alphabet == "кириллица":
        shift = shift % 33
        for char in string:
            if char.isalpha():
                if char.islower():
                    if ord(char) - shift >= ord("а"):
                        if ord(char) == 1105:
                            decode_string.append(chr(1078 - shift))
                        else:
                            decode_string.append(chr(ord(char) - shift))
                    else:
                        if ord(char) == 1105:
                            decode_string.append(
                                chr(ord("я") - (ord("а") - (1078 - shift) - 1))
                            )
                        else:
                            decode_string.append(
                                chr(ord("я") - (ord("а") - (ord(char) - shift) - 1))
                            )
                elif char.isupper():
                    if ord(char) - shift >= ord("А"):
                        if ord(char) == 1023:
                            decode_string.append(chr(1046 - shift))
                        else:
                            decode_string.append(chr(ord(char) - shift))
                    else:
                        if ord(char) == 1023:
                            decode_string.append(
                                chr(ord("Я") - (ord("А") - (1046 - shift) - 1))
                            )
                        else:
                            decode_string.append(
                                chr(ord("Я") - (ord("А") - (ord(char) - shift) - 1))
                            )
            else:
                decode_string.append(char)
    else:
        # пользователь ввел неподерживаемый алфавит
        return (
            "Такой алфавит не предусмотрен. Пожалуйста, попробуйте снова."
            "Для справки введите команду /help."
        )
    # возвращение зашифрованной строки
    return "".join(decode_string)
