import telebot

import cute_code_caesar
import cute_code_hamming
import cute_code_morse
import cute_code_transpon

cute_bot = telebot.TeleBot("token")


@cute_bot.message_handler(content_types=["text"])
def start(message):
    if message.text == "/start":
        cute_bot.send_message(
            message.from_user.id,
            "Привет! Покодируем? Выберите кодировку для работы:\n"
            "Код Морзе: /morse\n"
            "Код Хэмминга: /hamming\n"
            "Шифр Цезаря: /caesar\n"
            "Шифр транспонирования: /transp",
        )
    elif message.text == "/help":
        document = open("help.txt", "rb")
        cute_bot.send_document(message.from_user.id, document)
        document.close()
    elif message.text == "/menu":
        cute_bot.send_message(
            message.from_user.id,
            "Выберите кодировку для работы:\n"
            "Код Морзе: /morse\n"
            "Код Хэмминга: /hamming\n"
            "Шифр Цезаря: /caesar\n"
            "Шифр транспонирования: /transp",
        )
    elif message.text == "/hamming":
        cute_bot.send_message(
            message.from_user.id,
            "Введите режим работы:\n" "Кодировать: /code\n" "Раскодировать: /decode",
        )
        cute_bot.register_next_step_handler(message, get_hamming_mode)
    elif message.text == "/caesar":
        cute_bot.send_message(
            message.from_user.id,
            "Введите режим работы:\n" "Кодировать: /code\n" "Раскодировать: /decode",
        )
        cute_bot.register_next_step_handler(message, get_caesar_mode)
    elif message.text == "/morse":
        cute_bot.send_message(
            message.from_user.id,
            "Введите режим работы:\n" "Кодировать: /code\n" "Раскодировать: /decode",
        )
        cute_bot.register_next_step_handler(message, morse_mode)
    elif message.text == "/transp":
        cute_bot.send_message(
            message.from_user.id,
            "Введите режим работы:\n" "Кодировать: /code\n" "Раскодировать: /decode",
        )
        cute_bot.register_next_step_handler(message, get_transp_mode)
    else:
        cute_bot.send_message(
            message.from_user.id,
            "Извините, я не знаю такой команды. Для справки нажмите /help.",
        )


def get_hamming_mode(message):
    global hamming_mode
    if message.text == "/code":
        hamming_mode = "кодировать"
        cute_bot.send_message(
        message.from_user.id,
        "В каком формате Вы хотите получить полученный код?\n"
        "Cообщение в чате: /message\n"
        "Txt-файл: /file\n",
        )
        cute_bot.register_next_step_handler(message, get_hamming_format)
    elif message.text == "/decode":
        hamming_mode = "раскодировать"
        cute_bot.send_message(
        message.from_user.id,
        "В каком формате Вы хотите получить полученный код?\n"
        "Cообщение в чате: /message\n"
        "Txt-файл: /file\n",
        )
        cute_bot.register_next_step_handler(message, get_hamming_format)
    else:
        cute_bot.send_message(message.from_user.id, "Неправильный формат ввода.")

def get_hamming_format(message):
    global hamming_format
    if message.text == "/message":
        hamming_format = "/message"
        cute_bot.send_message(message.from_user.id, "Введите сообщение.")
        cute_bot.register_next_step_handler(message, get_hamming_result)
    elif message.text == "/file":
        hamming_format = "/file"
        cute_bot.send_message(message.from_user.id, "Введите сообщение.")
        cute_bot.register_next_step_handler(message, get_hamming_result)
    else:
        cute_bot.send_message(message.from_user.id, "Неправильный формат ввода.")


def get_hamming_result(message):
    result = cute_code_hamming.hamming(message.text, hamming_mode)
    if hamming_format == "/message":
        cute_bot.send_message(message.chat.id, result)
    elif hamming_format == "/file":
        document = open("hamming.txt", "w")
        document.write(result)
        document.close()
        document = open("hamming.txt", "rb")
        cute_bot.send_document(message.from_user.id, document)
        document.close()
    else:
        cute_bot.send_message(message.from_user.id, "Неправильный формат ввода.")


def get_caesar_mode(message):
    global caesar_mode
    if message.text == "/code":
        caesar_mode = "кодировать"
        cute_bot.send_message(
        message.from_user.id,
        "Выберите алфавит:\n" "Латиница: /latin\n" "Кириллица: /cyrillic",
        )
        cute_bot.register_next_step_handler(message, get_caesar_alphabet)
    elif message.text == "/decode":
        caesar_mode = "раскодировать"
        cute_bot.send_message(
        message.from_user.id,
        "Выберите алфавит:\n" "Латиница: /latin\n" "Кириллица: /cyrillic",
        )
        cute_bot.register_next_step_handler(message, get_caesar_alphabet)
    else:
        cute_bot.send_message(message.from_user.id, "Неправильный формат ввода.")


def get_caesar_alphabet(message):
    global caesar_alphabet
    if message.text == "/latin":
        caesar_alphabet = "латиница"
        cute_bot.send_message(message.from_user.id, "Введите числовое значение сдвига.")
        cute_bot.register_next_step_handler(message, get_caesar_shift)
    elif message.text == "/cyrillic":
        caesar_alphabet = "кириллица"
        cute_bot.send_message(message.from_user.id, "Введите числовое значение сдвига.")
        cute_bot.register_next_step_handler(message, get_caesar_shift)
    else:
        cute_bot.send_message(message.from_user.id, "Неправильный формат ввода.")


def get_caesar_shift(message):
    global caesar_shift
    if not message.text.isnumeric():
        cute_bot.send_message(message.from_user.id, "Неправильный формат ввода.")
    else:
        caesar_shift = int(message.text)
        cute_bot.send_message(
            message.from_user.id,
            "В каком формате Вы хотите получить полученный код?\n"
            "Cообщение в чате: /message\n"
            "Txt-файл: /file\n",
        )
        cute_bot.register_next_step_handler(message, get_caesar_format)


def get_caesar_format(message):
    global format
    if message.text == "/message":
        format = "/message"
        cute_bot.send_message(message.from_user.id, "Введите сообщение.")
        cute_bot.register_next_step_handler(message, get_caesar_result)
    elif message.text == "/file":
        format = "/file"
        cute_bot.send_message(message.from_user.id, "Введите сообщение.")
        cute_bot.register_next_step_handler(message, get_caesar_result)
    else:
        cute_bot.send_message(message.from_user.id, "Неправильный формат ввода.")


def get_caesar_result(message):
    result = cute_code_caesar.caesar(
        message.text, caesar_mode, caesar_shift, caesar_alphabet
    )
    if format == "/message":
        cute_bot.send_message(message.from_user.id, result)
    elif format == "/file":
        document = open("caesar.txt", "w")
        document.write(result)
        document.close()
        document = open("caesar.txt", "rb")
        cute_bot.send_document(message.from_user.id, document)
        document.close()


def morse_mode(message):
    global mode
    if message.text == "/code":
        mode = "кодировать"
        cute_bot.send_message(
            message.from_user.id,
            "На каком языке языке будет ваше сообщение?\n"
            "Латиница: /latin\n"
            "Кириллица: /cyrillic"
        )
        cute_bot.register_next_step_handler(message, language_morse)

    elif message.text == "/decode":
        mode = "раскодировать"
        cute_bot.send_message(
            message.from_user.id,
            "Выберите алфавит для дальнейшей записи:\n"
            "Латиница: /latin\n"
            "Кириллица: /cyrillic"
        )
        cute_bot.register_next_step_handler(message, language_morse)

    else:
        cute_bot.send_message(message.from_user.id, "Неправильный формат ввода.")


@cute_bot.message_handler(content_types=["text"])
def language_morse(message):
    global language
    text_for_code = "Введите сообщение. Допустимые символы:латиница или кириллица, цифры и знаки пунктуации."
    text_for_decode = "Введите сообщение. Допустимые символы: точка и дефис. Слова должны быть разделены " \
                      "табуляцией/двойным пробелом, символы -- пробелами. Убедитесь в правильности вашего сообщения. " \
                      "Те последовательности, которые не соотвествуют символам азбуки Морзе не будут переведены."
    if message.text == '/latin':
        language = 'латиница'
        if mode == "кодировать":
            cute_bot.send_message(
                message.from_user.id,
                text_for_code)
            cute_bot.register_next_step_handler(message, get_final_format_morse)
        elif mode == "раскодировать":
            cute_bot.send_message(
                message.from_user.id,
                text_for_decode)
            cute_bot.register_next_step_handler(message, get_final_format_morse)
        else:
            cute_bot.send_message(message.from_user.id, "Неправильный формат ввода.")
    elif message.text == '/cyrillic':
        language = 'кириллица'
        if mode == "кодировать":
            cute_bot.send_message(
                message.from_user.id,
                text_for_code)
            cute_bot.register_next_step_handler(message, get_final_format_morse)
        elif mode == "раскодировать":
            cute_bot.send_message(
                message.from_user.id,
                text_for_decode)
            cute_bot.register_next_step_handler(message, get_final_format_morse)
        else:
            cute_bot.send_message(message.from_user.id, "Неправильный формат ввода.")
    else:
        cute_bot.send_message(message.from_user.id, "Неправильный формат ввода.")


def get_final_format_morse(message):
    global strochka
    strochka = message.text
    if mode == "кодировать":
        cute_bot.send_message(
            message.from_user.id,
            "В каком формате Вы хотите получить полученный код?\n"
            "Cообщение в чате: /message\n"
            "Txt-файл: /file\n"
            "Аудиозапись в формате wav: /wav_file",
        )
        cute_bot.register_next_step_handler(message, get_function)
    elif mode == "раскодировать":
        cute_bot.send_message(
            message.from_user.id,
            "В каком формате Вы хотите получить расшифрованную "
            "последовательность?\n"
            "Cообщение в чате: /message\n"
            "Txt-файл: /file\n",
        )
        cute_bot.register_next_step_handler(message, get_function)
    else:
        cute_bot.send_message(message.from_user.id, "Неправильный формат ввода.")


def get_function(message):
    global final_format
    if message.text == "/message":
        final_format = "сообщение в чате"
        ans = cute_code_morse.get_function(mode, strochka, language, final_format)
        return cute_bot.send_message(message.from_user.id, ans)
    elif message.text == "/file":
        final_format = "txt-файл"
        cute_code_morse.get_function(mode, strochka, language, final_format)
        document = open("morse.txt", "rb")
        cute_bot.send_document(message.from_user.id, document)
        document.close()
    elif message.text == "/wav_file":
        final_format = "аудиозапись в формате wav"
        cute_code_morse.get_function(mode, strochka, language, final_format)
        document = open("morse.wav", "rb")
        cute_bot.send_audio(message.from_user.id, document, timeout=50)
        document.close()
    else:
        cute_bot.send_message(message.from_user.id, "Неправильный формат ввода.")


def get_transp_mode(message):
    global transp_mode
    if message.text == "/code":
        transp_mode = "кодировать"
        cute_bot.send_message(message.from_user.id, "Введите числовое значение сдвига.")
        cute_bot.register_next_step_handler(message, get_transp_shift)
    elif message.text == "/decode":
        transp_mode = "декодировать"
        cute_bot.send_message(message.from_user.id, "Введите числовое значение сдвига.")
        cute_bot.register_next_step_handler(message, get_transp_shift)
    else:
        cute_bot.send_message(message.from_user.id, "Неправильный формат ввода.")


def get_transp_shift(message):
    global transp_shift
    if not message.text.isnumeric():
        cute_bot.send_message(message.from_user.id, "Неправильный формат ввода.")
    else:
        transp_shift = int(message.text)
        cute_bot.send_message(
            message.from_user.id,
            "В каком формате Вы хотите получить полученный код?\n"
            "Cообщение в чате: /message\n"
            "Txt-файл: /file\n",
        )
        cute_bot.register_next_step_handler(message, get_transp_format)


def get_transp_format(message):
    global transp_format
    if message.text == "/message":
        transp_format = "/message"
        cute_bot.send_message(message.from_user.id, "Введите сообщение.")
        cute_bot.register_next_step_handler(message, transp_result)
    elif message.text == "/file":
        transp_format = "/file"
        cute_bot.send_message(message.from_user.id, "Введите сообщение.")
        cute_bot.register_next_step_handler(message, transp_result)
    else:
        cute_bot.send_message(message.from_user.id, "Неправильный формат ввода.")


def transp_result(message):
    result = cute_code_transpon.transp(message.text, transp_mode, transp_shift)
    if transp_format == "/message":
        cute_bot.send_message(message.from_user.id, result)
    elif transp_format == "/file":
        document = open("transp.txt", "w")
        document.write(result)
        document.close()
        document = open("transp.txt", "rb")
        cute_bot.send_document(message.from_user.id, document)
        document.close()
    else:
        cute_bot.send_message(message.from_user.id, "Неправильный формат ввода.")


cute_bot.polling(none_stop=True, interval=0)
